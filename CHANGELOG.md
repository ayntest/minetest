## 2014-10-29

Merges:

  - Add percent indicator to media loading. #1616
  - More consistent progress bar from 0-100 on startup #1578 (modified)
